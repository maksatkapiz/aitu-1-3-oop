package kz.aitu.oop.examples;

import java.util.*;

public class Shape {
    private ArrayList<Point> points;

    public Shape(ArrayList<Point> points) {
        this.points = points;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public void addPoint(Point point) {
        points.add(point);
    }

    public double calculatePerimeter(Shape shape) {
        double perimeter = 0;
        for (int i = 1; i < points.size() - 1; i++) {
            perimeter += points.get(i).calculateDistance(points.get(i - 1));
        }
        perimeter += points.get(points.size() - 1).calculateDistance(points.get(0));
        return perimeter;
    }

    public double longestSide(Shape shape) {
        double maxVal = 0;
        for (int i = 1; i < points.size() - 1; i++) {
            if (maxVal < points.get(i).calculateDistance(points.get(i - 1)))
                maxVal = points.get(i).calculateDistance(points.get(i - 1));
        }
        if (maxVal < points.get(points.size() - 1).calculateDistance(points.get(0)))
            maxVal = points.get(points.size() - 1).calculateDistance(points.get(0));
        return  maxVal;
    }

    public double avgLength(Shape shape) {
        return calculatePerimeter(shape) / shape.points.size();
    }
}